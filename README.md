# Ansible role for Memcached installation

## Introduction

[Memcached](http://www.memcached.org/) is a memory object caching
system, running as a daemon.

This role installs and configure the server.

You can specify the listening address (default to 127.0.0.1 to be
secure). The firewall configuration is left to your care.

## Variables

- **memcached_listen_address**: restrict listening socket (defaults to 127.0.0.1)
- **memcached_port**: connection port (defaults to 11211)
- **memcached_max_connections**: maximum simultaneous connections
- **memcached_cache_size**: maximum memory in MB for object storage

